# Project "PhoneBook on gRPC"

## Данный репозиторий содержит самостойтельный проект на GO

### Условия проекта
1. Проект создать на Cobra
2. Использовать ProtoBuf для сириализации данных
3. Использовать BoltDB для хранения данных в базе данных
4. Использовать gRPC для создания клиент-серверного приложения
5. Покрыть тестами процедуры на сервере
6. Использовать GoConvey

### Инструкция по созданию проекта
1. Установка необходимых инструментов:
    - установка Cobra. В терминале
    <pre>
    go get github.com/spf13/cobra/cobra
    </pre>
    Подробо о том, как создать проект на Cobra можно ознакомится по [*ссылке*] (https://gitlab.com/jaroslav.kuchmij/phoneBookCobraSruct)
    - установка ProtoBuf. В терминале:
    <pre>
    go get github.com/golang/protobuf/protoc-gen-go
    </pre>
    Подробо о том, как создать проект c использованием ProtoBuf можно ознакомится по [*ссылке*] (https://gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf)
    - установка BoltDB. В терминале
    <pre>
    go get github.com/boltdb/bolt/...
    </pre>
    - установк gRPC. В терминале
    <pre>
    go get google.golang.org/grpc
    </pre>
    - установка GoConvey. В терминале
    <pre>
    go get github.com/smartystreets/goconvey
    </pre>
2. Создать 2 проектf COBRA - серверную и клиентскую часть. Перейти в терминале %GOPATH%\bin и набрать команды:
    <pre>
    cobra init %GOPATH%/[Путь к проекту]/client 
    </pre>
    <pre>
    cobra init %GOPATH%/[Путь к проекту]/server 
    </pre>
3. Cоздать proto файл в пакете models с структурой для работы в клиент-серверном приложении в корне проекта. Пример структуры:
    <pre>
    syntax = "proto3";
package models;
service PhoneBook {
	rpc GetAllPhonebook(ContactKeyword) returns (stream ContactFullRequest) {}
	rpc AddContact (ContactFullRequest) returns (ContactResponse) {}
	rpc DeleteContact (NumberRequest) returns (ContactResponse) {}
	rpc SearchContact (NumberRequest) returns (ContactFullRequest) {}
	rpc UpdateName (UpdateNameRequest) returns (ContactResponse) {}
	rpc UpdateAddress (UpdateAddressRequest) returns (ContactResponse) {}
}
message ContactKeyword {
	string keyword = 1;
}
message ContactFullRequest {
	string number = 1;
	string name = 2;
    message Address {
        string street = 1;
        string numberHouse = 2;
        string numberFlat = 3;
    }
    Address address = 3;
}
message ContactResponse {
	string number = 1;
	bool status = 2;
}
message NumberRequest {
	string number = 1;
}
message UpdateNameRequest {
	string number = 1;
	string name = 2;
}
message UpdateAddressRequest {
	string number = 1;
	message Address {
            string street = 1;
            string numberHouse = 2;
            string numberFlat = 3;
        }
    Address address = 2;
}
    </pre>
4. Создание файла go с файла proto. Перейти в $GOPATH/bin/protoc-3.5.0-win32/bin и запустить комманду:
    <pre>
    protoc -I=E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookGRPC.git --go_out=plugins=grpc:E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookGRPC.git E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookGRPC.git\models\phoneBook.proto
    </pre>
5. В пакете server необходимо создать cobra - команды: StartServer и StopServer
    В StartServer находятся процедуры которые добавляют, удаляют, изменяют запись в БД и исчет записи в БД.
6. В пакете client необходимо создать cobra - команды, через которые пользователь будет отправлять необходимые запросы на сервер
    и передавать параметры
7. Создание тестов. Создать в пакете с проектом пакет tests и в нем создать тесты, которые покрываю процедуры на сервере

### Запуск проекта
1. Скачать проект - 
<pre>
go get gitlab.com/jaroslav.kuchmij/phoneBookGRPC.git
</pre>
2. Перейти в пакет:  %GOPATH%/src/gitlab.com/jaroslav.kuchmij/phoneBookDRPC.git/server и выполнить команду:
<pre>
go run main.go startServer
</pre>
После чего, необходимо ввести:
    - My.db - если запускаем боевую базу данных
    - Test.db - если запускаем тестовую БД
3. После чего открыть еще один терминал и перейти в пакет: %GOPATH%/src/gitlab.com/jaroslav.kuchmij/phoneBookDRPC.git/client и выполнить необходимые команды:
<pre>
go run main.go <command>
</pre>
Где <command>:
    - addContact - добавление контакта (Формат ввода: addContact +380981111111 Name Street NumHouse NumFlat)
    - searchContact - поиск контакта по номеру телефона (Формат ввода: searchContact +380981111111)
    - getContacts - получение всего списка контактов (Формат ввода: getContacts)
    - updateName - изменение имени по телефону (Формат ввода: updateNam +380981111111 NewName)
    - updateAddress - изменение адреса по номеру телефона (Формат ввода: updateAddress +380981111111 NewStreet NewNumHouse NewNumFlat)
    - deleteContact - удаление контакта по номеру телефона (Формат ввода: deleteContact +380981111111)
4. Для того чтобы остановить сервер, необходимо перейти в терминал где он запущен и выполнить комбинацию клавиш "ctrl+C"
5. Вызов тестов: Перейти в пакет %GOPATH%/src/gitlab.com/jaroslav.kuchmij/phoneBookDRPC.git/tests и запустить команду:
<pre>
go test -v
</pre>

### gRPC

[*Документация*](https://godoc.org/google.golang.org/grpc)

### GoConvey

[*Ресурс*](https://goconvey.co/)

**~~Создатель кода~~** Главный бездельник - [*YaroslavKuchmiy*](https://www.facebook.com/profile.php?id=100006520172879)