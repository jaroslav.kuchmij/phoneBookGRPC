package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"gitlab.com/jaroslav.kuchmij/phoneBookGRPC.git/utils"
	"google.golang.org/grpc"
	"gitlab.com/jaroslav.kuchmij/phoneBookGRPC.git/models"
	"context"
)

func init() {
	rootCmd.AddCommand(searchContactCmd)
}

// Команда поиск контакта по номеру телефона
var searchContactCmd = &cobra.Command{
	Use:   "searchContact",
	Short: "search Contact into data base on the server",
	Long:  `This command searching Contact into data base on the server by phone namber
	Format enter: searchContact +380981111111`,
	Run: func(cmd *cobra.Command, args []string) {
		//Создание канала для связи с сервером
		conn, err := grpc.Dial(address, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("did not connect: %v", err)
		}
		defer conn.Close()

		//Клиентская заглушка
		client := models.NewPhoneBookClient(conn)

		//Проверка входящих аргументов
		if len(args) != 1 {
			log.Println("Invalid contact")
			return
		}

		//Запись входящих данных в структуру
		number := &models.NumberRequest{
			Number: args [0],
		}

		//Передача параметров процедуре на сервере и получение ответа
		resp, err := client.SearchContact(context.Background(), number)
		if err != nil {
			log.Println("error in deleteContact():", err)
			return
		}
		if resp.Number == "" {
			log.Println("This number does not exist:", args[0])
			return
		}

		utils.PrintStructContact(resp)
	},
}