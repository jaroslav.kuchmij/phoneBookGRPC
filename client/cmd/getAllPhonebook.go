package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"google.golang.org/grpc"
	"gitlab.com/jaroslav.kuchmij/phoneBookGRPC.git/models"
	"context"
	"gitlab.com/jaroslav.kuchmij/phoneBookGRPC.git/utils"
	"io"
)

func init() {
	rootCmd.AddCommand(getContactsCmd)
}

// Команда чтения всех элементов БД на сервере
var getContactsCmd = &cobra.Command{
	Use:   "getContacts",
	Short: "get all contacts",
	Long:  `This command reads items into data base on the server and print this items on the screen

	Format enter: getContacts`,
	Run: func(cmd *cobra.Command, args []string) {
		//Создание канала для связи с сервером
		conn, err := grpc.Dial(address, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("did not connect: %v", err)
		}
		defer conn.Close()

		//Клиентская заглушка
		client := models.NewPhoneBookClient(conn)

		key := &models.ContactKeyword{Keyword: ""}
		//Передача параметров процедуре на сервере и получение потока
		stream, err := client.GetAllPhonebook(context.Background(), key)
		if err != nil {
			log.Fatalf("Error on get customers: %v", err)
		}
		//Обработка потока двнных
		for {
			contact, err := stream.Recv() //Получение элемента из потока
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("%v.GetCustomers(_) = _, %v", client, err)
			}

			//Вывод полученных данных на экран
			utils.PrintStructContact(contact)
		}
	},
}