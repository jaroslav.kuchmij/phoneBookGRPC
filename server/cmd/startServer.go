package cmd

import (
	"github.com/spf13/cobra"
	"google.golang.org/grpc"
	"net"
	"log"
	"gitlab.com/jaroslav.kuchmij/phoneBookGRPC.git/models"
	"golang.org/x/net/context"
	"gitlab.com/jaroslav.kuchmij/phoneBookGRPC.git/utils"
	"github.com/boltdb/bolt"
	"github.com/golang/protobuf/proto"
	"errors"
	"fmt"
)

type server struct {}
const(
	port = ":50051"
)
var nameDB string

func init() {
	rootCmd.AddCommand(startServerCmd)
}

// Команда запуска сервера
var startServerCmd = &cobra.Command{
	Use:   "startServer",
	Short: "start grpc server",
	Long:  `This command starting grpc server.`,
	Run: func(cmd *cobra.Command, args []string) {
		//Выбор базы данных
		fmt.Println("Enter the name data base. Enter 'Test.db' if you run tests and 'My.db' if you run a combat DB")
		fmt.Scanln(&nameDB)
		if nameDB != "Test.db" && nameDB != "My.db"  {
			log.Fatalf("Not a valid name data base")
		}
		//Открытие порта
		lis, err := net.Listen("tcp", port)
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}
		// Creates a new gRPC server
		s := grpc.NewServer()
		models.RegisterPhoneBookServer(s, &server{})
		if err := s.Serve(lis); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
		log.Println("Start server")

		_, err = bolt.Open(nameDB, 0600, nil) //открытие БД
		if err != nil{
			log.Println("error in Open DB: ", err)
			return
		}
	},
}

//Добавление контакта. Возврат true если котакт добавлен, false, если такой контакт уже есть
func (s *server) AddContact(ctx context.Context, in *models.ContactFullRequest) (*models.ContactResponse, error)  {
	log.Println("Add contact")
	db, err := bolt.Open(nameDB, 0600, nil) //открытие БД
	if err != nil{
		return nil, err
	}
	defer db.Close() //После выполнения команды - закрытие БД
	utils.CreateBucketInDB(db)
	if err != nil {
		return nil, err
	}
	if !utils.CheckValidNumber(in.Number) {		//проверка на формат номера
		return nil, errors.New("Invalid number")
	}

	//проверка на существование номера в файле.
	if bool, err, _ := utils.SearchNumberInDB(in.Number, db); bool == true {
		if err != nil {
			return nil, err
		}
		return &models.ContactResponse{Number: in.Number, Status: false}, nil
	}
	err = utils.MarshalAndWriteInDB(in, db) //Маршал структуры и запись в БД
	if err != nil {
		return nil, err
	}
	return &models.ContactResponse{Number: in.Number, Status: true}, nil
}

//Получение всех контактов в БД. Возвращает поток
func (s *server) GetAllPhonebook(key *models.ContactKeyword, stream models.PhoneBook_GetAllPhonebookServer) error {
	log.Println("Get all contacts")
	contact := &models.ContactFullRequest{}
	db, err := bolt.Open(nameDB, 0600, nil) //открытие БД
	if err != nil{
		return err
	}
	defer db.Close()  //После выполнения команды - закрытие БД

	err = db.View(func(tx *bolt.Tx) error { //Открытие корзины для просмотра
		book := tx.Bucket([]byte(utils.NameBucket))
		book.ForEach(func(k, v []byte) error { //Итерация полученной корзины
			err = proto.Unmarshal(v, contact) //анмаршал значение корзины
			if err != nil{
				return err
			}
			//Передача потока данных
			if err := stream.Send(contact); err != nil {
				return err
			}
			return nil
		})
		return nil
	})
	if err != nil {
		return err
	}
	return nil
}

//Удаление контакта. Возврат Статуса true, если удаление прошло успешно, иначе не нашел контакт
func (s *server) DeleteContact(ctx context.Context, in *models.NumberRequest) (*models.ContactResponse, error) {
	log.Println("Delete contact")
	db, err := bolt.Open(nameDB, 0600, nil) //открытие БД
	if err != nil{
		return nil, err
	}
	defer db.Close() //После выполнения команды - закрытие БД

	if !utils.CheckValidNumber(in.Number) {		//проверка на формат номера
		return nil, errors.New("Invalid number")
	}

	//проверка на существование номера в файле.
	if bool, err, _ := utils.SearchNumberInDB(in.Number, db); bool == false {
		if err != nil {
			return nil, err
		}
		return &models.ContactResponse{Number: in.Number, Status: false}, nil
	}
	err = db.Update(func(tx *bolt.Tx) error { //зменение корзины
		bckt := tx.Bucket([]byte(utils.NameBucket)) //Получение корзины по наименованию
		err := bckt.Delete([]byte(in.Number)) //Удаление записи в корзине по ключу
		return err
	})
	if err != nil {
		return nil, err
	}
	return &models.ContactResponse{Number: in.Number, Status: true}, nil
}

//Поиск контакта. Возврат полной струтктуры, если совпадение есть, иначе возврат пустой структуры
func (s *server) SearchContact(ctx context.Context, in *models.NumberRequest) (*models.ContactFullRequest, error) {
	log.Println("Search contact")
	db, err := bolt.Open(nameDB, 0600, nil) //открытие БД
	if err != nil{
		return &models.ContactFullRequest{}, err
	}
	defer db.Close()  //После выполнения команды - закрытие БД

	if !utils.CheckValidNumber(in.Number) {		//проверка на формат номера
		return nil, errors.New("Invalid number")
	}

	if bool, err, item := utils.SearchNumberInDB(in.Number, db); bool == true { //проверка на существование номера в файле
		if err != nil {
			return &models.ContactFullRequest{}, err
		}
		return item, nil
	}
	return &models.ContactFullRequest{}, nil
}

//Изменение имени контакта. Возврат статуса true, если котакт найден и изменен, иначе контакт не найден
func (s *server) UpdateName(ctx context.Context, in *models.UpdateNameRequest) (*models.ContactResponse, error) {
	log.Println("Update name contact")
	db, err := bolt.Open(nameDB, 0600, nil) //открытие БД
	if err != nil{
		return &models.ContactResponse{}, err
	}
	defer db.Close() //После выполнения команды - закрытие БД

	if !utils.CheckValidNumber(in.Number) {		//проверка на формат номера
		return nil, errors.New("Invalid number")
	}

	if bool, err, item := utils.SearchNumberInDB(in.Number, db); bool == true { //проверка на существование номера в файле
		item = &models.ContactFullRequest{ //Запись входящих данных в структуру
			Number: item.Number,
			Name: in.Name,
			Address: item.Address,
		}
		err = utils.MarshalAndWriteInDB(item, db) //Маршал вх данных и запись из в БД
		if err != nil {
			return &models.ContactResponse{}, err
		}
		return &models.ContactResponse{Number: in.Number, Status: true}, nil
	}
	if err != nil {
		return &models.ContactResponse{}, err
	}
	return &models.ContactResponse{Number: in.Number, Status: false}, nil
}

//Изменение фдркса контакта. Возврат статуса true, если котакт найден и изменен, иначе контакт не найден
func (s *server) UpdateAddress(ctx context.Context, in *models.UpdateAddressRequest) (*models.ContactResponse, error) {
	log.Println("Update address contact")
	db, err := bolt.Open(nameDB, 0600, nil) //открытие БД
	if err != nil{
		return &models.ContactResponse{}, err
	}
	defer db.Close() //После выполнения команды - закрытие БД

	if !utils.CheckValidNumber(in.Number) {		//проверка на формат номера
		return nil, errors.New("Invalid number")
	}

	if bool, err, item := utils.SearchNumberInDB(in.Number, db); bool == true { //проверка на существование номера в файле
		item = &models.ContactFullRequest{ //Запись входящих данных в структуру
			Number: item.Number,
			Name: item.Name,
			Address: &models.ContactFullRequest_Address{
				Street: in.Address.Street,
				NumberHouse: in.Address.NumberHouse,
				NumberFlat: in.Address.NumberFlat,
			},
		}
		err = utils.MarshalAndWriteInDB(item, db) //Маршал вх данных и запись из в БД
		if err != nil {
			return &models.ContactResponse{}, err
		}
		return &models.ContactResponse{Number: in.Number, Status: true}, nil
	}
	if err != nil {
		return &models.ContactResponse{}, err
	}
	return &models.ContactResponse{Number: in.Number, Status: false}, nil
}