package cmd

import (
	"github.com/spf13/cobra"
	"log"

	"google.golang.org/grpc"
	"gitlab.com/jaroslav.kuchmij/phoneBookGRPC.git/models"
	"context"
)

func init() {
	rootCmd.AddCommand(updateNameCmd)
}

// Команда изменения имение контакта по номеру телефона 
var updateNameCmd = &cobra.Command{
	Use:   "updateName",
	Short: "update Name into data base on the server",
	Long:  `This command updating name contact on the server.
	Searching occurs on phone namber
	Format enter: updateName +380981111111 NewName`,
	Run: func(cmd *cobra.Command, args []string) {
		//Создание канала для связи с сервером
		conn, err := grpc.Dial(address, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("did not connect: %v", err)
		}
		defer conn.Close()

		//Клиентская заглушка
		client := models.NewPhoneBookClient(conn)

		//Проверка входящих аргументов
		if len(args) != 2 {
			log.Println("Invalid contact")
			return
		}

		//Запись входящих данных в структуру
		updContact := &models.UpdateNameRequest{
			Number: args [0],
			Name: args[1],
		}

		//Передача параметров процедуре на сервере и получение ответа
		resp, err := client.UpdateName(context.Background(), updContact)
		if err != nil {
			log.Println("error in updateName():", err)
			return
		}
		if resp.Status == false {
			log.Println("This number does not exist:", resp.Number)
			return
		}
		log.Println("Update contact:", resp.Number)
	},
}