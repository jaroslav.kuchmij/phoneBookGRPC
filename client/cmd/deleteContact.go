package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"google.golang.org/grpc"
	"gitlab.com/jaroslav.kuchmij/phoneBookGRPC.git/models"
	"context"
)


func init() {
	rootCmd.AddCommand(deleteContactCmd)
}

// Команда удаления контакта по номеру телефона 
var deleteContactCmd = &cobra.Command{
	Use:   "deleteContact",
	Short: "delete contact into data base on the server",
	Long:  `This command deleting contact in data base on the server.
	Searching occurs on phone namber
	Format enter: deleteContact +380981111111`,
	Run: func(cmd *cobra.Command, args []string) {
		//Создание канала для связи с сервером
		conn, err := grpc.Dial(address, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("did not connect: %v", err)
		}
		defer conn.Close()

		//Клиентская заглушка
		client := models.NewPhoneBookClient(conn)

		//Проверка входящих аргументов
		if len(args) != 1 {
			log.Println("Invalid contact")
			return
		}

		//Запись входящих данных в структуру
		number := &models.NumberRequest{
			Number: args [0],
		}

		//Передача параметров процедуре на сервере и получение ответа
		resp, err := client.DeleteContact(context.Background(), number)
		if err != nil {
			log.Println("error in deleteContact():", err)
			return
		}
		if resp.Status == false {
			log.Println("This number does not exist:", resp.Number)
			return
		}
		log.Println("Delte contact:", resp.Number)
	},
}