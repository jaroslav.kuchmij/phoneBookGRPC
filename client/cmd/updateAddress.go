package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"google.golang.org/grpc"
	"gitlab.com/jaroslav.kuchmij/phoneBookGRPC.git/models"
	"context"
)

func init() {
	rootCmd.AddCommand(updateAddressCmd)
}

// Команда изменения адреса контакта по номеру телефона 
var updateAddressCmd = &cobra.Command{
	Use:   "updateAddress",
	Short: "update Address into data base on the server",
	Long:  `This command updating Address contact on the server.
	Searching occurs on phone namber
	Format enter: updateAddress +380981111111 NewStreet NewNumberHouse NewNumberFlat`,
	Run: func(cmd *cobra.Command, args []string) {
		//Создание канала для связи с сервером
		conn, err := grpc.Dial(address, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("did not connect: %v", err)
		}
		defer conn.Close()

		//Клиентская заглушка
		client := models.NewPhoneBookClient(conn)

		//Проверка входящих аргументов
		if len(args) != 4 {
			log.Println("Invalid contact")
			return
		}

		//Запись входящих данных в структуру
		updContact := &models.UpdateAddressRequest{
			Number: args [0],
			Address: &models.UpdateAddressRequest_Address{
				Street: args[1],
				NumberHouse: args[2],
				NumberFlat: args[3],
			},
		}

		//Передача параметров процедуре на сервере и получение ответа
		resp, err := client.UpdateAddress(context.Background(), updContact)
		if err != nil {
			log.Println("error in updateAddress():", err)
			return
		}
		if resp.Status == false {
			log.Println("This number does not exist:", resp.Number)
			return
		}
		log.Println("Update contact:", resp.Number)
	},
}