package utils

import (
	"regexp"
	"gitlab.com/jaroslav.kuchmij/phoneBookGRPC.git/models"
	"github.com/golang/protobuf/proto"
	"github.com/boltdb/bolt"
	"fmt"
)

const NameBucket = "PhoneBook"

//Создание корзины если ее нет
func CreateBucketInDB(db *bolt.DB) error {
	err := db.Update(func(tx *bolt.Tx) error { //изменение БД
		_, err := tx.CreateBucketIfNotExists([]byte(NameBucket)) //создание Bucket
		if err != nil {
			return err
		}
		return err
	})
	if err != nil {
		return err
	}
	return err
}

//Сириализация
func MarshalAndWriteInDB(contact *models.ContactFullRequest, db *bolt.DB)  error {
	out, err := proto.Marshal(contact) //перевод Структуры в формат ProtoBuf (сирилизация)
	if err != nil{
		return err
	}
	err = db.Update(func(tx *bolt.Tx) error { //Изменение БД
		err = tx.Bucket([]byte(NameBucket)).Put([]byte(contact.Number), out) //Получение необходимой корзины и добавление новой записи
		if err != nil {
			return err
		}
		return err
	})
	if err != nil {
		return err
	}
	return err
}

//Поиск совпадений по номеру
func SearchNumberInDB(incomingValue string, db *bolt.DB)  (bool, error, *models.ContactFullRequest) {
	boolSwitch := false // если Истина - совпадение есть, иначе нет совпадений
	contact := models.ContactFullRequest{}
	err := db.View(func(tx *bolt.Tx) error { //Открытие БД для чтения
		if val := tx.Bucket([]byte(NameBucket)).Get([]byte(incomingValue)); val != nil { //Получение значения по ключу
			err := proto.Unmarshal(val, &contact) //Анмаршал значения и запись в структуру данных
			if err != nil{
				panic(err)
			}
			boolSwitch = true // если есть совпадение указываем Истину
			return nil
		}
		return nil
	})
	if err != nil {
		panic(err)
	}
	return boolSwitch, err, &contact
}

//Проверка входящего номера на корректность
func CheckValidNumber(incomingValue string) bool {
	var validID = regexp.MustCompile(`^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$`) //формат номера
	res := validID.MatchString(incomingValue)
	return res
}

//Вывод структуры телефонной книги на экран
func PrintStructContact(contact *models.ContactFullRequest)  {
	fmt.Println("Contact number: ", contact.Number)
	fmt.Println("Contact name: ", contact.Name)
	fmt.Println("Contact address: ", contact.Address.Street+" "+contact.Address.NumberHouse+" "+contact.Address.NumberFlat)
	fmt.Println("-------------------------------------------------------")
}