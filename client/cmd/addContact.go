package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"gitlab.com/jaroslav.kuchmij/phoneBookGRPC.git/models"
	"google.golang.org/grpc"

	"context"
)

const (
	address = "localhost:50051"
)

func init() {
	rootCmd.AddCommand(addContactCmd)
}

var addContactCmd = &cobra.Command{
	Use:   "addContact",
	Short: "add contact from data base",
	Long:  `This command add phone number, name and address into data base.

	Format enter: addContact +380111111111 Yaroslav Voenomorskaya 3 4
	Format phone: +380111111111
	Format Name: free form
	AddressStreet: Voenomorskaya
	AddressHouseNumber: 3
	AddressFlatNumber: 4`,
	Run: func(cmd *cobra.Command, args []string) {
		//Создание канала для связи с сервером
		conn, err := grpc.Dial(address, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("did not connect: %v", err)
		}
		defer conn.Close()

		//Клиентская заглушка
		client := models.NewPhoneBookClient(conn)

		//Проверка входящих аргументов
		if len(args) != 5 {
			log.Println("Invalid contact")
			return
		}

		//Запись входящих данных в структуру
		contact := &models.ContactFullRequest{
			Number: args[0],
			Name: args[1],
			Address: &models.ContactFullRequest_Address{
				Street: args[2],
				NumberHouse: args[3],
				NumberFlat: args[4],
			},
		}
		//Передача параметров процедуре на сервере и получение ответа
		resp, err := client.AddContact(context.Background(), contact)
		if err != nil {
			log.Println("error in addContact():", err)
			return
		}
		if resp.Status == false {
			log.Println("This number exist:", resp.Number)
			return
		}
		log.Println("Add contact:", resp.Number)
	},
}